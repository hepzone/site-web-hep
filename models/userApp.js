/**
 * Created by mathias on 23/03/2015.
 */

module.exports = function (socket, mongoose) {

    var Schema = mongoose.Schema;
    mongoose.models = {};
    mongoose.modelSchemas = {};
    var UserSchema = new Schema({
        id: String,
        created: String,
        email: String,
        code:String,
        lastSeen: String,
        username: String,
        position: {},
        profile: {},
        stats: {}
    });

    var Users = mongoose.model('Users', UserSchema);

   var getUpdate = function () {
        Users.find({}, function (err, docs) {
            if (err) throw err;
            socket.emit("sync:Users", docs);
        })
    };

    getUpdate();
    socket.on("Update:Users", function(){
        getUpdate();
    });
    socket.on("save:User", function (user) {
        delete user._id;
        Users.update({id: user.id},{username: user.profile.name, email:
            user.email,id: user.id, code:user.code, position:user.position, profile:user.profile, lastSeen:user.lastSeen, created:user.created}, { upsert: true },function(err){
            if (err) {
                throw err;
            }
            getUpdate();
        })
    });

    socket.on("Get:User", function (userId) {
        Users.find({id: userId}, function(err, Users){
            if (err)
                throw err;
            socket.emit("Get:User", Users);
            getUpdate();
        });
    });

    socket.on("were:Heped", function (Id) {
        Users.findOne({id:Id},'stats', function(err, stats){
            if (err) throw (err);
            socket.emit("wereHeped", stats);
            socket.on("wereHeped", function(stats){
                Users.update({id:Id}, {'stats':stats}, { upsert: true }, function(err){
                    if (err){
                        throw err;
                    }
                   getUpdate();
                })
            })
        })
    });
}