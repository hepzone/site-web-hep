/**
 * Created by mathias on 23/03/2015.
 */
module.exports = function(socket, mongoose)
{
    var Schema = mongoose.Schema;
    mongoose.models = {};
    mongoose.modelSchemas = {};

    var NotifSchema = new Schema({
        date: String,
        from: String,
        to:String,
        read: Boolean,
        text: String,
        text2: String,
        event:String
    });

    var Notif = mongoose.model('Notif', NotifSchema);

    socket.on("Update:Notifs", function (userId) {
        Notif.find({to:userId},function (err, notifs) {
            if (err) throw err;
            socket.emit("sync:Notifs", notifs);
        });
    });

    var Update = function(to) {
        Notif.find({to: to}, function (err, notifs) {
            if (err) throw err;
            socket.broadcast.emit("sync:Notifs", notifs);
        });
        };

    socket.on("save:Notif", function (notif) {
        var notif = new Notif({from:notif.from, to:notif.to,
            read:notif.read, text:notif.text, text2:notif.text2, date:notif.date,event:notif.event})
        notif.save(function(err){
            if (err) throw err;
            Update(notif.to);
        })
    });

    socket.on("update:Notif", function (notif) {
        Notif.update({_id:notif._id},{read:notif.read},function(err){
            if (err) throw err;
            socket.emit("Update:Notifs", notif.to);
        })
    });
};