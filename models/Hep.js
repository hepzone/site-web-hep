/**
 * Created by mathias on 23/03/2015.
 */
module.exports = function(io, mangoose)
{
    io.sockets.on('connection', function(socket){
        console.log("connect");

        var Schema = mongoose.Schema;
        mongoose.models = {};
        mongoose.modelSchemas = {};

        var EventSchema = new Schema({
            created: {type: Date, default: Date.now},
            start: Date,
            end: Date,
            code:String,
            champ: String,
            champa: String,
            champb: String,
            champc: String,
            colorchamp2: {},
            fileUrl: String,
            name: String,
            id: String,
            rep: {},
            creator: String,
            repa: {},
            currentUser: Number,
            MaxUser: Number,
            position:{}
        });

        var UserSchema = new Schema({
            id: String,
            created: String,
            code:String,
            lastSeen: Number,
            position: {},
            profile: {},
            stats: {}
        });

        var NotifSchema = new Schema({
            date: String,
            from: String,
            to:String,
            read: Boolean,
            text: String,
            text2: String,
            event:String
        });

        var Notif = mongoose.model('Notif', NotifSchema);
        var Users = mongoose.model('Users', UserSchema);
        var Evt = mongoose.model('Event', EventSchema);

        var getUpdateEvent = function() {
            Evt.find({}, function (err, docs) {
                if (err) throw err;
                count(docs);
                io.sockets.emit("sync:Events", docs);
            });
        };

        getUpdateEvent();
        socket.on("Update:Events", function(){
            getUpdateEvent();
        });

        var count = function(event) {
            Users.find({}, function (err, users) {
                if (err) throw err;
                for (var y in event) {
                    var z = 0;
                    for (var x in users) {
                        if (event[y].code == users[x].code && (Date.now() - users[x].lastSeen < 2 * 60 * 60 * 1000)) {
                            z++;
                        }
                    }
                    Evt.update({id: event[y].id}, {currentUser: z}, {upsert: true}, function (err) {
                        if (err) {
                            throw err;
                        }
                    });
                }
            });
        };

        socket.on("Save:Event", function (Event) {
            console.log(Event);
            var event = new Evt({id: Event.id, name: Event.name, Start:Event.start,
                End: Event.end, code:Event.code, champ:Event.champ, champa:Event.champa, champb:Event.champb, champc:Event.champc,
                colorchamp2:Event.colorchamp2, rep:Event.rep, creator:Event.creator, repa:Event.repa, position:Event.position,fileUrl: Event.fileUrl,
                currentUser:Event.currentUser, MaxUser:Event.MaxUser});
            event.save(function (err) {
                if (err) throw (err);
            })
        });

        socket.on("Get:Event", function (id) {
            Evt.find({id:id}, function(err, Event){
                if (err)
                    throw err;
                io.sockets.emit("Get:Event", Event);
            });
        });

        var getUpdateUser = function () {
            Users.find({}, function (err, docs) {
                if (err) throw err;
                io.sockets.emit("sync:Users", docs);
            })
        };

        getUpdateUser();
        socket.on("Update:Users", function(){
            getUpdateUser();
        });
        socket.on("save:User", function (user) {
            if(user.lastSeen == undefined){
                user.lastSeen = Date.now();
            }
            Users.update({id: user.id},{id: user.id, code:user.code, position:user.position, profile:user.profile, lastSeen:user.lastSeen, created:user.created}, { upsert: true },function(err){
                if (err) {
                    throw err;
                }
                getUpdateUser();
            })
        });

        socket.on("Get:User", function (userId) {
            Users.find({id: userId}, function(err, Users){
                if (err)
                    throw err;
                socket.emit("Get:User", Users);
                getUpdateUser();
            });
        });

        socket.on("were:Heped", function (Id) {
            Users.findOne({id:Id},'stats', function(err, stats){
                if (err) throw (err);
                socket.emit("wereHeped", stats);
                socket.on("wereHeped", function(stats){
                    Users.update({id:Id}, {'stats':stats}, { upsert: true }, function(err){
                        if (err){
                            throw err;
                        }
                        getUpdateUser();
                    })
                })
            })
        });

        var Update = function() {
            Notif.find({}, function (err, notifs) {
                if (err) throw err;
                io.sockets.emit("sync:Notifs", notifs);
            });
        };

        Update();
        socket.on("save:Notif", function (notif) {
            var notif = new Notif({from:notif.from, to:notif.to,
                read:notif.read, text:notif.text, text2:notif.text2, date:notif.date,event:notif.event});
            notif.save(function(err){
                if (err) throw err;
                Update();
            })
        });

        socket.on("update:Notif", function (notif) {
            Notif.update({_id:notif._id},{read:notif.read},function(err){
                if (err) throw err;
              Update();
            })
        });

        //require("./userApp.js")(socket, mangoose);
        //require("./Event.js")(socket, mangoose);
        //require("./NotifApp.js")(socket, mangoose);
    })
}
