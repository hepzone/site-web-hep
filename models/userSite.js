/**
 * Created by mathias on 23/03/2015.
 */
    var mongoose = require('mongoose');
    var bcrypt = require('bcrypt');
    SALT_WORK_FACTOR = 10;
    var Schema = mongoose.Schema;
    mongoose.models = {};
    mongoose.modelSchemas = {};
    PassportLocalStrategy = require('passport-local').Strategy;

    var UserSchema = new Schema({
        username: {type: String, required:true, trim: true},
        firstname: {type: String, required:true, trim: true},
        lastname: {type: String, required:true, trime: true},
        email: {type:String, required: true, trim: true, lowercase:true, unique: true},
        number:{type:String},
        password: {type:String, required:true},
        admin: {type: Boolean, required: true},
        created:{type: Date, default: Date.now}
    });



UserSchema.pre('save', function(next){
    var user = this;

    if (!user.isModified('password')) return next();

    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
        if (err) return next(err);

        bcrypt.hash(user.password, salt, function(err, hash){
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.comparePassword = function(candidatePassword, cb){
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    })
};
mongoose.model('UserSite', UserSchema);




