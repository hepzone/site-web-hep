/**
 * Created by mathias on 23/03/2015.
 */
module.exports = function(socket, mongoose)
{
    var Schema = mongoose.Schema;
    mongoose.models = {};
    mongoose.modelSchemas = {};

    var EventSchema = new Schema({
        created: {type: Date, default: Date.now},
        Start: Date,
        End: Date,
        code:String,
        champ: String,
        champa: String,
        champb: String,
        champc: String,
        colorchamp2: {},
        name: String,
        id: String,
        rep: {},
        creator: String,
        repa: {},
        currentUser: String,
        MaxUser: String,
        position:{}
    });

    var Evt = mongoose.model('Event', EventSchema);

   var getUpdate = function() {
       Evt.find({}, function (err, docs) {
           if (err) throw err;
           console.log(docs);
           socket.emit("sync:Events", docs);
       });
   };

    getUpdate();
    socket.on("Update:Events", function(){
        getUpdate();
    });

    socket.on("count:Event", function(users, event){
        for (var y in event) {
            var z = 0;
            for (var x in users) {
                if (event[y].code == users[x].code) {
                    z++;
                }
            }
            Evt.update({id: event[y].id}, {currentUser: z}, {upsert: true}, function (err) {
                if (err) {
                    throw err;
                }
                getUpdate();
            });
        }
    });

    socket.on("Save:Event", function (Event) {
        var event = new Evt({id: Event.id, name: Event.name, start:Event.start,
            end: Event.end, code:Event.code, champ:Event.champ, champa:Event.champa, champb:Event.champb, champc:Event.champc,
            colorchamp2:Event.colorchamp2, rep:Event.rep, creator:Event.creator, repa:Event.repa, position:Event.position,
            currentUser:Event.currentUser, MaxUser:Event.MaxUser});
        event.save(function (err) {
            if (err) throw (err);
        })
    });

    socket.on("Get:Event", function (id) {
        Evt.find({id:id}, function(err, Event){
            if (err)
                throw err;
            socket.emit("Get:Event", Event);
        });
    });
};