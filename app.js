/**
 * Created by mathias on 23/03/2015.
 */
var express = require('express');
var    app = express();
var server = require('http').createServer(app);
var morgan = require('morgan');
var session = require('express-session');
var LocalStrategy = require('passport-local').Strategy;
var bodyParser = require('body-parser');
var io = require('socket.io').listen(server);
var passport = require('passport');
var user = require('./models/userSite');
mongoose = require('mongoose');
var User = mongoose.model('UserSite');
var fs = require('fs');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

var MongoStore = require('connect-mongo')(session);
app.use(session({
        store: new MongoStore({url: 'mongodb://localhost/hep'}),
        secret: 'supernova',
        resave:true,
        saveUninitialized:true}
));

mongoose.connect('mongodb://localhost/hep', function(err)
{
    if (err) {
        throw err;
    }
    else {
        console.log('connect to the database')
    }
});
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.set('view cache', false);
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy({usernameField:'email',
                                passwordField:'password'},
    function(username, password, done){
    User.findOne({email: username}, function(err, user){
        if (err){
            return done(err)
        }
        if (!user){
            return done(null, false, {alert:'Incorrect username.'});
        }
       user.comparePassword(password, function(err, isMatch){
           if (isMatch){
               return done(null, user);
           } else {
               return done(null, false, {message:'Incorrect password.'});
           }
       })
    })
}));
passport.serializeUser(function(user, done){
    done(null, user.id);
});
passport.deserializeUser(function(id, done){
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

function isAuthenticated(req, res, next){
    if (req.isAuthenticated())return next();
        res.sendStatus(401);
}

app.post('/auth/login', passport.authenticate('local'), function(req, res){
   res.json(req.user);
});

app.get('/auth/currentuser', isAuthenticated, function(req, res){
    res.json(req.user);
});

app.post('/auth/signup', function(req, res){
    var u = new User();
    console.log(req);
    u.username = req.body.email;
    u.password = req.body.password;
    u.email = req.body.email;
    u.lastname = req.body.lastname;
    u.firstname = req.body.firstname;
    u.number = req.body.number;
    u.admin = false;

    u.created = req.body.created;

    u.save(function(err){
        if (err){
            res.json({'alert': 'Registration error'});
        } else {
            res.json({'alert': 'Registration success'});
        }
    });
});

app.get('/auth/logout', function(req, res){
    console.log('logout');
    req.logout();
    res.sendStatus(200);
});

app.post('/upload', multipartMiddleware, function(req, res) {
    var n = req.files;
    fs.rename(n.file.path, '/var/www/site-web-hep/public/upload/' + n.file.originalFilename, function (err) {
        if (err) throw err;
        fs.stat('/var/www/site-web-hep/public/upload/' + n.file.originalFilename, function (err, stats) {
            if (err) throw err;
            console.log('stats: ' + JSON.stringify(stats));
        });
    });
});

app.get('/bann/:name', function (req, res) {
    res.send('<img src="http://hep-zone.com/upload/'+ req.param("name") +'">');
});


app.use(express.static("public", __dirname + "/public"));
require("./routes/routes.js")(app);
app = require("./models/Hep.js")(io, mongoose);

exports.express = app;
exports.mongoose = mongoose;

server.listen(8090);

