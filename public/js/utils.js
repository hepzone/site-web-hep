angular.module('hepZone')

.factory('Utils', function(){
  'use strict';
  var service = {
    createId: createId
  };

  function createId(){
    function S4(){ return (((1+Math.random())*0x100)|0).toString(16).substring(1); }
    return (S4() + '-' + S4() + '-4' + S4().substr(0,3) + '-' + S4() + '-' + S4() + S4() + S4()).toLowerCase();
  }

  return service;
});