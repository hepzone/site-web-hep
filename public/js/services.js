angular.module('hepZone')

.factory('EventSrv', function($http, $firebase, firebaseUrl, Utils, socket, $q){
  'use strict';
  var service = {
    syncEvent: syncEvent,
    get: get,
    save: save,
    getId: getId
  };

  function syncEvent(){
      var defer = $q.defer();
      socket.on("sync:Events",function(res){
          defer.resolve(res);
      })
      return defer.promise;
  }

  function get(EventId){
    socket.emit("Get:Event", EventId)
      var defer = $q.defer();
    socket.on("Get:Event",function(res){
        defer.resolve(res);
    })
      return defer.promise;
  }

  function save(Event){
      console.log(Event);
    socket.emit("Save:Event", Event);
  }
  
  function getId(Event){
	  if (Event && Event.id)
	  return (Event.id);
	  else {
		return (Utils.createId());
	  }
  }
	return service;
  })

.factory('PlugSrv', function($q, StorageSrv, Utils){
	function getDeviceId(){
    var defer = $q.defer();
    $ionicPlatform.ready(function(){
      var device = ionic.Platform.device();
      if(device && device.uuid){
        defer.resolve(device.uuid);
      } else {
        var user = StorageSrv.get('user');
        defer.resolve(user && user.id ? user.id : Utils.createUuid());
      }
    });
    return defer.promise;
  }
})

.factory('Stock', function($window){
'use strict';
	var localStorageCache = {};
  	var localStoragePrefix = 'Event-';
  	var service = {
    	get: get,
    	set: set
  	};

  function get(key){
    if(!localStorageCache[key] && $window.localStorage){
      localStorageCache[key] = JSON.parse($window.localStorage.getItem(localStoragePrefix+key));
    }
    return angular.copy(localStorageCache[key]);
  }

  function set(key, value){
    if(!angular.equals(localStorageCache[key], value)){
      localStorageCache[key] = angular.copy(value);
      if($window.localStorage){
        $window.localStorage.setItem(localStoragePrefix+key, JSON.stringify(localStorageCache[key]));
      }
    }
  }

  return service;
})
.factory('socket', function($rootScope) {
var socket = io.connect("ws://hep-zone.com:8090");
    return {
    on: function (eventName, callback) {
        socket.on(eventName, function () {
            var args = arguments;
            $rootScope.$apply(function () {
                callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
})
