var hepZone = angular.module('hepZone', ['ngRoute','ngFileUpload','ui.bootstrap', 'firebase', 'colorpicker.module', 'ngMap']); //

hepZone.config(function($routeProvider){

	$routeProvider
.when('/', {
		templateUrl: 'main.html'
	})
.when('/params', {
			templateUrl: 'param.html'
		})
.when('/registration', {
			templateUrl: 'registration.html'
		})
.when('/profil', {
	templateUrl: 'profil.html'
})
.otherwise({
	redirectTo: '/'	
});
})

.constant('firebaseUrl', 'https://hep-zone.firebaseio.com/'+(Config.debug ? '/dev' : '')+'/site');
