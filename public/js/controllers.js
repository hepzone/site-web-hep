angular.module('hepZone')

hepZone.controller('mainController', function($scope){
});

hepZone.controller('CarouselDemoCtrl', function($scope){
   $scope.myInverval = 100;
    var slides = $scope.slides = [];
    $scope.addSlide = function(){
        var newWidth = 600 + slides.length + 1;
        slides.push({
            image: 'http://placekitten.com/' + newWidth + '/400',
            text:['More','Extra', 'Lots of', 'Surplus'][slides.length % 4] + ' ' +
            ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
        });
    };
    for (var i=0; i<4; i++) {
        $scope.addSlide();
    }
});

hepZone.controller('ParamController', function($scope){
});

hepZone.controller('ProfilController', function($scope, $http){
    $http.get('/auth/currentuser').
        success(function (data) {
            $scope.user = data;
        }).
        error(function () {
            $location.path('/signin');
        });

});

hepZone.controller('registrationController', function($scope, $http){
  $scope.user = {};

    $scope.register = function(user){
        user.created = Date.now();
        $http.post("/auth/signup", user).success(function(data){
            $scope.alert = data.alert;
        }).error(function(){
            console.log("lol");
        });
    }
});

hepZone.controller("ConnectCtrl", function ($scope, $location, $http) {
    $http.get('/auth/currentuser').
        success(function (data) {
            $scope.loggerduser = data;
        }).
        error(function () {
    });
    $scope.login = function(user){
        $http.post('/auth/login', user).success(function(data){
            $scope.loggerduser = data;
            $location.path('/params');
        }).error(function(){
            $scope.alert = 'login failed'
        });
    }
    $scope.logout = function () {
        $http.get('/auth/logout').success(function () {
            $scope.loggerduser = null;
            $location.path('/#');
            location.reload();
        }).error(function(){
            $scope.alert= 'Logout failed'
        });
    }
});

hepZone.controller('formController', function($scope, Upload, EventSrv,Stock, Utils, socket, $http, $location){
    $http.get('/auth/currentuser').
        success(function (data) {
                $scope.loggeduser = data;
    }).
    error(function () {
    $location.path('/signin');
    });
    $scope.search = '';
    $scope.$on('mapInitialized', function (evt, evtMap) {
        var map = evtMap;
        var marker = map.markers[0];
        var shape = map.shapes.circle;
        $scope.position = marker.getPosition();
        $scope.position.radius = shape.getRadius();
        navigator.geolocation.getCurrentPosition(function(position) {
                initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(initialLocation);
                marker.setPosition(initialLocation);
                shape.setCenter(initialLocation);
            });
        $scope.pos = function (e){
            marker.setPosition(e.latLng);
            shape.setCenter(e.latLng);
            $scope.position = marker.getPosition();
            $scope.position.radius = shape.getRadius();
            console.log(marker.getPosition());
        };
    });
    $scope.reps = ['Reponse possible champ 1 : 1'];
	$scope.addrep = function (){
		var newRepNo = $scope.reps.length + 1;
		$scope.reps.push('Reponse possible champ 1 : ' + newRepNo);
	};
	$scope.repsa = ['Reponse possible champ 2 : 1'];
	$scope.addrepa = function () {
        var newRepNoa = $scope.repsa.length + 1;
        $scope.repsa.push('Reponse possible champ 2 : ' + newRepNoa);
    };
    socket.on("sync:Events",function(res){
        $scope.data.eventOther = res;
    });
	$scope.data = {
                events: Stock.get('events'),
				saving: false,
                message: "Soumettre"
	};

        $scope.create = function(){
            $scope.data.events.id = EventSrv.getId($scope.data.events);
        };
	$scope.save = function(event, position) {
            var events = event;
            var tab = [];

            if ($scope.data.file){
                Upload.upload({
                    url: 'http://hep-zone.com/upload/',
                    fileName: Utils.createId() + '.jpg',
                    method: 'POST',
                    file: $scope.data.file
                });
                events.fileUrl = 'http://hep-zone.com/upload/' + events.id + '.jpg';
            }
            events.position = [];
			$scope.data.saving = true;
            $scope.data.message = "Chargement...";
            setTimeout(function () {
        	$scope.$apply(function () {
			$scope.data.saving = false;
        	});
    		}, 2000);
            $scope.data.message = "Soumettre";
			events.id = EventSrv.getId($scope.data.events) || null;
            tab.push(position.F); //longitude
            tab.push(position.A); //latitude
            tab.push(position.radius); // radius
            events.position = {
                longitude: position.G,
                latitude: position.K,
                radius: position.radius
            };
            console.log(events.position);
            events.creator = $scope.loggeduser.id;
            events.code = events.id;
			EventSrv.save(events);
            //window.location.reload();
    }
}
);
